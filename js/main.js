 /*@constructor*/
function Hamburger(size, stuffing) {
    try {
        if (size && stuffing && arguments.length) {
            this.sizeProp = size;
            this.stuffProp = stuffing;
            this.topping = [];
        } else {
            throw new HamburgerException('Mistake in arguments of Hamburger function');
        }
    } catch (error) {
        console.error(error.name + ": " + error.message);
    }
}

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {size: 'small', price: 50, calories: 20};
Hamburger.SIZE_LARGE = {size: 'large', price: 100, calories: 40};
Hamburger.STUFFING_CHEESE = {name: 'cheese', price: 10, calories: 20};
Hamburger.STUFFING_SALAD = {name: 'salad', price: 20, calories: 5};
Hamburger.STUFFING_POTATO = {name: 'potato', price: 15, calories: 10};
Hamburger.TOPPING_MAYO = {name: 'mayo', price: 20, calories: 5};
Hamburger.TOPPING_SPICE = {name: 'spice', price: 15, calories: 0};

/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.*/
Hamburger.prototype.addTopping = function (topping) {
    try {
        if (!this.topping.includes(topping)) {
            this.topping.push(topping);
        } else {
            throw new HamburgerException('Duplicate topping')
        }
    } catch (error) {
        console.error(error.name + ": " + error.message);
    }
};

/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 */
Hamburger.prototype.removeTopping = function (topping) {
    try {
        if (this.topping.includes(topping)) {
            this.topping.splice(this.topping.indexOf(topping), 1);
        } else {
            throw new HamburgerException('This toppting dont added');
        }
    } catch (error) {
        console.error(error.name + ": " + error.message);
    }
};

/**
 * Получить список добавок.
 */
Hamburger.prototype.getToppings = function () {
    const array = [];
    for (let i = 0; i < this.topping.length; i++) {
        array.push(this.topping[i].name)
    }
    return array;
};
/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function () {
    return this.sizeProp.size;
};

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
    return this.stuffProp.name
};

/**
 * Узнать цену гамбургера
 */
Hamburger.prototype.calculatePrice = function () {
    let price = this.sizeProp.price + this.stuffProp.price;
    let toppingsArr = this.topping;
    for (let i = 0; i < toppingsArr.length; i++) {
        price += toppingsArr[i].price;
    }
    return price
};

/**
 * Узнать калорийность
 */
Hamburger.prototype.calculateCalories = function () {
    let calories = this.sizeProp.calories + this.stuffProp.calories;
    let toppingsArr = this.topping;
    for (let i = 0; i < toppingsArr.length; i++) {
        calories += toppingsArr[i].calories;
    }
    return calories
};


let hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);
hamburger.addTopping(Hamburger.TOPPING_SPICE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// hamburger.addTopping(Hamburger.TOPPING_SPICE);
// hamburger.addTopping(Hamburger.TOPPING_SPICE);
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
// hamburger.removeTopping(Hamburger.TOPPING_SPICE);

// hamburger.removeTopping(Hamburger.TOPPING_MAYO);
console.log('Toppings:', hamburger.getToppings());
console.log('Size: %s', hamburger.getSize());
console.log('Stuffing: %s', hamburger.getStuffing());
console.log('Price: %d', hamburger.calculatePrice());
console.log('Calories: %d', hamburger.calculateCalories());
console.log("Have %d toppings", hamburger.getToppings().length);

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 */
function HamburgerException(message) {
    this.message = message;
    this.name = "HamburgerException";
}
